CXXFLAGS=-Wall -pedantic -std=c++11
LDFLAGS=`pkg-config --libs allegro{,_dialog,_primitives}-5`
VPATH=core:entities

.PHONY: clean

main: ball.o brick.o main.o game.o paddle.o
	$(CXX) $(LDFLAGS) $^ -o $@

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c $^

ball.o paddle.o: collidable.hpp
brick.o: entity.hpp

%.o: %.cpp %.hpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	$(RM) main *.o
