# Block Kuzushi

> A small block-breaker game

[![Licence][licence-badge]][licence-url]

This project is a clone of the classic block-breaker game [Arkanoid][game-url].

## Installation

:bulb: Requires [Allegro 5](https://liballeg.org/download.html).

Clone the project like this:

```sh
git clone https://gitlab.com/joakimaling/block-kuzushi.git
```

## Usage

Run this code to compile and run the game:

```sh
make && ./main
```

## Licence

Released under GNU-3.0. See [LICENSE][licence-url] for more.

Coded with :heart: by [joakimaling][user-url].

[game-url]: https://en.wikipedia.org/wiki/Arkanoid
[licence-badge]: https://badgen.net/gitlab/license/joakimaling/block-kuzushi
[licence-url]: LICENSE
[user-url]: https://gitlab.com/joakimaling
