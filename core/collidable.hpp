#pragma once

#include "entity.hpp"

namespace kuzushi {
	class Collidable: public Entity {
		protected:
			int dx = 0, dy = 0;

		public:
			Collidable(const int x, const int y, const int w, const int h):
				Entity(x, y, w, h) {}
			virtual ~Collidable() {}

			/**
			 * Checks if another entity collides with this entity and, if so,
			 * returns true, otherwise false.
			 *
			 * @param  entity An entity to use in the collision detection
			 * @return        True if they collide, false if not
			 */
			bool hits(Entity *entity) const {
				return
					this->y + this->height >= entity->y &&
					this->y <= entity->y + entity->height &&
					this->x + this->width >= entity->x &&
					this->x <= entity->x + entity->width;
			}

			/**
			 * All collidables move and can thus collide.
			 */
			virtual void tick() = 0;
	};
}
