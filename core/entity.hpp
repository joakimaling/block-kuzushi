#pragma once

namespace kuzushi {
	class Entity {
		public:
			int height, width, x, y;

			Entity(const int x, const int y, const int w, const int h):
				height(h), width(w), x(x), y(y) {}
			virtual ~Entity() {}

			/**
			 * All entities can be drawn.
			 */
			virtual void draw() = 0;
	};
}
