#include "game.hpp"

namespace kuzushi {
	/**
	 * Creates the display onto which to draw the graphics, the event queue and
	 * timer which will keep the game running at the same speed across systems.
	 */
	Game::Game() {
		if (!al_init()) {
			throw std::runtime_error("Couldn't initialise Allegro");
		}

		if (!(display = al_create_display(width * scale, height * scale))) {
			throw std::runtime_error("Couldn't create the display");
		}

		if (!(eventQueue = al_create_event_queue())) {
			throw std::runtime_error("Couldn't create the event queue");
		}

		if (!(timer = al_create_timer(1.0 / 60.0))) {
			throw std::runtime_error("Couldn't create the timer");
		}

		if (!al_init_primitives_addon()) {
			throw std::runtime_error("Couldn't initialise the primitives");
		}

		if (!al_install_mouse()) {
			throw std::runtime_error("Couldn't install the mouse");
		}

		ALLEGRO_TRANSFORM transform;
		al_identity_transform(&transform);
		al_scale_transform(&transform, scale, scale);
		al_use_transform(&transform);

		ALLEGRO_MONITOR_INFO monitor;
		al_get_monitor_info(0, &monitor);

		// calculate the point of the upper left corner to centre the window
		int px = (monitor.x2 - monitor.x1 - al_get_display_width(display)) / 2;
		int py = (monitor.y2 - monitor.y1 - al_get_display_height(display)) / 2;

		al_set_window_position(display, px, py);
		al_set_window_title(display, (title + " v" + version).c_str());
		// al_set_display_icon(display, al_load_bitmap("assets/icon.png"));

		al_register_event_source(eventQueue, al_get_display_event_source(display));
		al_register_event_source(eventQueue, al_get_timer_event_source(timer));
		al_register_event_source(eventQueue, al_get_mouse_event_source());

		al_hide_mouse_cursor(display);
		al_start_timer(timer);

		loadLevel(levelCounter);

		paddle = new Paddle(this);
		ball = new Ball(this, paddle);
	}

	/**
	 * Destroys the display, event queue and timer used by the game to free up
	 * memory.
	 */
	Game::~Game() {
		al_destroy_event_queue(eventQueue);
		al_destroy_display(display);
		al_destroy_timer(timer);
	}

	/**
	 * Draws everything coming from the entities classes to the screen and flips
	 * the buffers.
	 */
	void Game::draw() {
		al_clear_to_color(al_map_rgb(0, 0, 0));

		for (Brick *brick: bricks) {
			brick->draw();
		}

		paddle->draw();
		ball->draw();

		al_flip_display();
	}

	/**
	 * Loads the level with the number indicated by the parameter into memory.
	 * This is done at game launch and when transitioning between levels.
	 *
	 * @param level A number indicating which level to load
	 */
	void Game::loadLevel(const int level) {
		std::stringstream ss;
		ss << "levels/level" << level << ".txt";
		std::ifstream ifs(ss.str());
		char type;

		const ALLEGRO_COLOR colours[10] = {
			al_map_rgb(252, 252, 252), // 0 - white
			al_map_rgb(252, 116, 96),  // 1 - orange
			al_map_rgb(60, 188, 252),  // 2 - light blue
			al_map_rgb(128, 208, 16),  // 3 - green
			al_map_rgb(216, 40, 0),    // 4 - red
			al_map_rgb(0, 112, 236),   // 5 - blue
			al_map_rgb(252, 116, 180), // 6 - pink
			al_map_rgb(252, 152, 56),  // 7 - yellow
			al_map_rgb(188, 188, 188), // 8 - silver
			al_map_rgb(240, 188, 60)   // 9 - gold
		};

		breakableBricks = 0;
		bricks.clear();
		ifs >> type;

		for (int i = 0; !ifs.eof(); i++) {
			if (type != '.') {
				bricks.push_back(new Brick(
					type,
					colours[(int) (type - 48)],
					type > '8' ? 0 : (type > '7' ? 50 * level : ((int) (type - 48) * 10 + 50)),
					(i % LEVEL_WIDTH) * BRICK_WIDTH,
					(i / LEVEL_WIDTH) * BRICK_HEIGHT,
					BRICK_WIDTH,
					BRICK_HEIGHT
				));

				if (type < '9') {
					breakableBricks++;
				}
			}

			ifs >> type;
		}

		ifs.close();
	}

	/**
	 * Runs the game loop. Updates the logic and draws eveything to the screen
	 * by calling the game's tick and draw methods.
	 */
	void Game::run() {
		ALLEGRO_EVENT event;
		bool redraw = false;

		while (isRunning) {
			al_wait_for_event(eventQueue, &event);
			paddle->handleEvent(event);
			ball->handleEvent(event);

			switch (event.type) {
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					isRunning = false;
					break;
				case ALLEGRO_EVENT_DISPLAY_SWITCH_IN:
					isHidden = false;
					break;
				case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:
					isHidden = true;
					break;
				case ALLEGRO_EVENT_TIMER:
					if (!isHidden) {
						redraw = true;
						tick();
					}
			}

			// draw to the screen when all events have been processed
			if (redraw && al_is_event_queue_empty(eventQueue)) {
				redraw = false;
				draw();
			}
		}
	}

	/**
	 * Does all the game's logic and calculations. Checks collisions between the
	 * ball and the bricks and paddle, or if it flew outside of the level.
	 */
	void Game::tick() {
		paddle->tick();
		ball->tick();

		for (Brick *brick: bricks) {
			if (ball->hits(brick)) {
				ball->deflect();

				if (brick->isBreakable()) {
					paddle->score += brick->getValue();
					bricks.erase(std::remove(bricks.begin(), bricks.end(), brick));

					// if all breakable brick are broken, then move up one level
					if (--breakableBricks <= 0) {
						loadLevel(++levelCounter);
						paddle->reset();
						ball->reset();
					}
				}
			}
		}

		// the ball bounces on the paddle
		if (ball->hits(paddle)) {
			ball->deflect();
		}

		// the ball flew passed the paddle and out of the level
		if (ball->isLost()) {
			if (--paddle->lives > 0) {
				paddle->reset();
				ball->reset();
			}
			else {
				// death...
			}
		}
	}
}
