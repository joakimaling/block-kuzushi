#pragma once

#include <algorithm>
#include <allegro5/allegro.h>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include "../entities/ball.hpp"
#include "../entities/brick.hpp"
#include "../entities/paddle.hpp"

#define BRICK_HEIGHT 8
#define BRICK_WIDTH 16

#define LEVEL_HEIGHT 28
#define LEVEL_WIDTH 11

namespace kuzushi {
	class Ball;

	class Game {
		private:
			ALLEGRO_DISPLAY *display;
			ALLEGRO_EVENT_QUEUE *eventQueue;
			ALLEGRO_TIMER *timer;

			std::vector<Brick*> bricks;
			Paddle *paddle;
			Ball *ball;

			const std::string title = "ブロック崩し";
			const std::string version = "0.1.0";

			bool isRunning = true;
			bool isHidden = false;

			int breakableBricks;
			int levelCounter = 1;

			void draw();
			void loadLevel(const int);
			void tick();

		public:
			const int height = BRICK_HEIGHT * LEVEL_HEIGHT;
			const int width = BRICK_WIDTH * LEVEL_WIDTH;
			const int scale = 3;

			Game();
			~Game();

			void run();
	};
}
