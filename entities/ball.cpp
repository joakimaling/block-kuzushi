#include "ball.hpp"

namespace kuzushi {
	/**
	 * Initialises the ball. It saves a pointer to the paddle so the ball knows
	 * where the paddle is.
	 */
	Ball::Ball(Game *g, Paddle *p):
		Collidable(0, 0, SIZE, SIZE), game(g), paddle(p) {
		reset();
	}

	/**
	 * Centre the ball on the paddle.
	 */
	void Ball::centre() {
		x = paddle->x + (paddle->width - SIZE) / 2;
	}

	/**
	 * [Ball::deflect description]
	 */
	void Ball::deflect() {
		dy = -dy;
	}

	/**
	 * Draws the ball onto the screen.
	 */
	void Ball::draw() {
		al_draw_filled_rounded_rectangle(x, y, x + SIZE, y + SIZE, SIZE / 2,
			SIZE / 2, al_map_rgb(252, 152, 56));
	}

	/**
	 * Handles events coming from the mouse.
	 *
	 * @param event Information about an event
	 */
	void Ball::handleEvent(const ALLEGRO_EVENT event) {
		if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			if (event.mouse.button == 1) {
				isAttached = false;
			}
		}
	}

	/**
	 * Returns true if the ball is outside of the boundaries of the game
	 *
	 * @return True if the ball is lost, otherwise false
	 */
	bool Ball::isLost() const {
		return isOutside;
	}

	/**
	 * Resets the ball's position on top of the paddle.
	 */
	void Ball::reset() {
		centre();
		y = paddle->y - SIZE;
		isAttached = true;
		isOutside = false;
		dx = -1;
		dy = -1;
	}

	/**
	 * Updates the position of the ball on the screen.
	 */
	void Ball::tick() {
		if (isAttached) {
			centre();
		}
		else {
			x += dx;
			y += dy;

			if (x < 0 || x > game->width - SIZE) {
				dx = -dx;
			}

			if (y < 0) {
				dy = -dy;
			}
			else if (y > game->height - SIZE) {
				isOutside = true;
			}
		}
	}
}
