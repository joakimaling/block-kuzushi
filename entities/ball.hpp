#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "../core/collidable.hpp"
#include "../core/game.hpp"
#include "paddle.hpp"

#define SIZE 4

namespace kuzushi {
	class Game;
	class Paddle;

	class Ball: public Collidable {
		private:
			Game *game;
			Paddle *paddle;

			bool isAttached;
			bool isOutside;

			void centre();

		public:
			Ball(Game*, Paddle*);
			~Ball() {}

			void deflect();
			void draw();
			void handleEvent(const ALLEGRO_EVENT);
			bool isLost() const;
			void reset();
			void tick();
	};
}
