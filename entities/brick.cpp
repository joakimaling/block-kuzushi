#include "brick.hpp"

namespace kuzushi {
	/**
	 * Draws the brick onto the screen.
	 */
	void Brick::draw() {
		al_draw_rectangle(x, y, x + width, y + height, BLACK, 1);
		al_draw_filled_rectangle(x, y, x + width - 1, y + height - 1, colour);

		if (type > '7') {
			al_draw_line(x + 1, y + height - 1, x + width - 1, y + height - 1, BLACK, 1);
			al_draw_line(x + width - 1, y + 1, x + width - 1, y + height - 1, BLACK, 1);
		}
	}

	/**
	 * Returns the value. This is done when the brick gets destroyed, adding its
	 * value to the player's score.
	 *
	 * @return The value of the brick
	 */
	int Brick::getValue() const {
		return value;
	}

	/**
	 * Returns true if the brick is breakable, otherwise false.
	 *
	 * @return True if the brick is breakable, otherwise false
	 */
	bool Brick::isBreakable() const {
		return type < '9';
	}
}
