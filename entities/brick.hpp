#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "../core/entity.hpp"
// #include "pill.hpp"

#define BLACK al_map_rgb(0, 0, 0)

namespace kuzushi {
	class Brick: public Entity {
		private:
			ALLEGRO_COLOR colour;
			char type;
			int value;

			// Pill *pill;

		public:
			Brick(const char t, const ALLEGRO_COLOR c, const int v, const int x, const int y, const int w, const int h):
				Entity(x, y, w, h), colour(c), type(t), value(v) {}
			~Brick() {}

			void draw();
			int getValue() const;
			bool isBreakable() const;
	};
}
