#include "paddle.hpp"

namespace kuzushi {
	/**
	 * Initialises the values of the paddle, creates its ball and resets them.
	 *
	 * @param game A pointer to the game object
	 */
	Paddle::Paddle(Game* g): Collidable(0, 0, 24, 4) {
		game = g;
		lives = 3;
		score = 0;
		reset();
	}

	/**
	 * Draws the paddle onto the screen.
	 */
	void Paddle::draw() {
		al_draw_filled_rounded_rectangle(x, y, x + width, y + height, 2, 2,
			al_map_rgb(188, 188, 188));
	}

	/**
	 * Handles events coming from the mouse.
	 *
	 * @param event Information about an event
	 */
	void Paddle::handleEvent(const ALLEGRO_EVENT event) {
		if (event.type == ALLEGRO_EVENT_MOUSE_AXES) {
			x = event.mouse.x;
		}
		else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
			if (event.mouse.button == 1) {
				isFiring = true;
			}
		}
		else if (event.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
			if (event.mouse.button == 1) {
				isFiring = false;
			}
		}
	}

	/**
	 * Resets the paddle's position in the middle of the screen and resets the
	 * ball's position.
	 */
	void Paddle::reset() {
		x = (game->width - width) / 2;
		y = game->height - height - 16;
	}

	/**
	 * Updates the position of the paddle on the screen.
	 */
	void Paddle::tick() {
		if (x < 0) {
			x = 0;
		}
		else if (x > game->width - width) {
			x = game->width - width;
		}
	}
}
