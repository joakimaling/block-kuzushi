#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "../core/collidable.hpp"
#include "../core/game.hpp"

namespace kuzushi {
	class Game;

	class Paddle: public Collidable {
		private:
			Game *game;

			bool isFiring = false;

		public:
			int lives, score;

			Paddle(Game*);
			~Paddle() {}

			void draw();
			void handleEvent(const ALLEGRO_EVENT);
			void reset();
			void tick();
	};
}
