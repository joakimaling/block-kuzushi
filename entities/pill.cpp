#include "pill.hpp"

namespace kuzushi {
	/**
	 * Draws the pill onto the screen.
	 */
	void Pill::draw() {
		al_draw_filled_rounded_rectangle(x, y, x + width, y + height, 0, 0, colour);
		al_draw_text(game->font, x, y, 0, ALLEGRO_TEXT_CENTRE, text);
	}

	/**
	 * Updates the position of the pill on the screen.
	 */
	void Pill::tick() {
		y += 5;
	}
}
