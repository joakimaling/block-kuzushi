#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "../core/collidable.hpp"

namespace kuzushi {
	class Game;

	class Pill: public Collidable {
		private:
			Game *game;

			char letter;

		public:
			Pill(const int x, const int y): Collidable(x, y, 12, 4) {}
			~Pill() {}

			void draw();
			void tick();
	};
}
