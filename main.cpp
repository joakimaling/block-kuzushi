#include <allegro5/allegro_native_dialog.h>
#include "core/game.hpp"

int main() {
	try {
		kuzushi::Game game;
		game.run();
	}
	catch (const std::runtime_error& e) {
		al_show_native_message_box(NULL, "Error", "Error", e.what(), NULL, 2);
	}

	return 0;
}
